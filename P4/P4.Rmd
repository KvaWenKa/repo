---
title: "P4"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Основы обработки данных с помощью R


## Цель

1. Развить практические навыки использования языка программирования R для обработки данных
2. Закрепить знания основных функций обработки данных экосистемы tidyverse языка R
3. Развить пркатические навыки использования функций обработки данных пакета dplyr – функции
select(), filter(), mutate(), arrange(), group_by()

## Задание

Проанализировать встроенные в пакет nycflights13 наборы данных с помощью языка R и ответить на вопросы.

## Содержание ПР
```{r}
library(nycflights13)
library(dplyr)
```
### 1. Сколько встроенных в пакет nycflights13 датафреймов?

5

### 2. Сколько строк в каждом датафрейме?
```{r}
nycflights13::airlines %>% nrow()
nycflights13::airports %>% nrow()
nycflights13::flights %>% nrow()
nycflights13::planes %>% nrow()
nycflights13::weather %>% nrow()
```
### 3. Сколько столбцов в каждом датафрейме?
```{r}
nycflights13::airlines %>% ncol()
nycflights13::airports %>% ncol()
nycflights13::flights %>% ncol()
nycflights13::planes %>% ncol()
nycflights13::weather %>% ncol()
```
### 4. Как просмотреть примерный вид датафрейма?
```{r}
nycflights13::airlines %>% glimpse()
nycflights13::airports %>% glimpse()
nycflights13::flights %>% glimpse()
nycflights13::planes %>% glimpse()
nycflights13::weather %>% glimpse()
```
### 5. Сколько компаний перевозчиков (carrier) учитывают эти наборы данных (представлено в наборах данных)?
```{r}
nycflights13::airlines %>% summarise(count = n_distinct(carrier)) %>% knitr::kable()
```
### 6. Сколько рейсов принял аэропорт John F Kennedy Intl в мае?
```{r}
nycflights13::flights %>% filter(month==5 & dest=='JFK') %>% count() %>% knitr::kable()
```
### 7. Какой самый северный аэропорт?
```{r}
nycflights13::airports %>% filter(lat == max(lat, na.rm = TRUE)) %>% select(name) %>% knitr::kable()
```
### 8. Какой аэропорт самый высокогорный (находится выше всех над уровнем моря)?
```{r}
nycflights13::airports %>% filter(alt == max(alt, na.rm = TRUE)) %>% select(name) %>% knitr::kable()
```
### 9. Какие бортовые номера у самых старых самолетов?
```{r}
year_old <- nycflights13::planes %>% select(year) %>% unique()%>% arrange(year) %>% head(1)%>%unlist()
nycflights13::planes %>% filter(year == year_old) %>% knitr::kable()
```
### 10. Какая средняя температура воздуха была в сентябре в аэропорту John F Kennedy Intl(в градусах Цельсия).
```{r}
t_fahr <- nycflights13::weather %>% filter(origin=='JFK' & month==9) %>% group_by(origin)%>% summarise(m=mean(temp)) %>% select(m)

5/9*(t_fahr-32) 
```
### 11. Самолеты какой авиакомпании совершили больше всего вылетов в июне?
```{r}
carrier_max_fl <- nycflights13::flights %>% filter(month==6) %>% group_by(carrier) %>% summarise(recorded = n())%>% arrange(recorded) %>% tail(1) %>% .$carrier
nycflights13::airlines %>% filter(carrier==carrier_max_fl) %>% select(name) %>% knitr::kable()
```
### 12. Самолеты какой авиакомпании задерживались чаще других в 2013 году?
```{r}
late_fl <- nycflights13::flights %>% filter(arr_delay>0 | dep_delay>0) %>% group_by(carrier)%>% summarise(recorded = n())%>% arrange(desc(recorded))

all_fl <- nycflights13::flights %>% group_by(carrier) %>% summarise(sum_fl = n()) %>% arrange(desc(sum_fl))

res <- left_join(all_fl, late_fl, by = 'carrier')

worst_carrier <- res %>% mutate(ratio = recorded / sum_fl) %>% arrange(ratio) %>% tail(1) %>% .$carrier

nycflights13::airlines %>% filter(carrier==worst_carrier) %>% select(name) %>% knitr::kable()
```