---
title: "P5"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Основы обработки данных с помощью R


## Цель

1. Развить практические навыки использования языка программирования R для обработки данных
2. Закрепить знания основных функций обработки данных экосистемы tidyverse языка R
3. Развить пркатические навыки использования функций обработки данных пакета dplyr – функции
select(), filter(), mutate(), arrange(), group_by()

## Задание

Вы администратор сетевой безопасности для среднего бизнеса XYZcorp. Вы часто используете данные сетевого потока для обнаружения аномальных событий безопасности. Эта задача предоставляет образцы агрегированных данных о потоках и использует ответы от аномальных событий для построения флага.

## Содержание ПР
```{r}
library(tidyverse)
list.files(path = "../input")
list.files(path = "../input", recursive = T)

library(vroom)
df <- vroom::vroom("../input/2019-trendmicro-ctf-wildcard-400/gowiththeflow_20190826.csv")
df <- vroom::vroom("../input/2019-trendmicro-ctf-wildcard-400/gowiththeflow_20190826.csv", col_names = F, 
                  col_types = c(X1 = "d",X2 = "c",X3 = "c",X4 = "i",X5 = "i"))
colnames(df)<-c('timestamp', 'src','dst', 'port', 'bytes')


library(lubridate)
library(dplyr)
options(digits.secs=3)
df <- df %>% mutate(rtime = lubridate::as_datetime(timestamp/1000)) %>% select(2:6)
```
### Вопрос 1: Наша интеллектуальная собственность покидает здание большими кусками. Машина внутри используется для отправки всех наших проектов виджетов. Один хост отправляет с предприятия гораздо больше данных, чем другие. Какой у него IP? 
```{r}
is_internal_host <- function(ip){
    stringr::str_detect(ip,"^1[2-4].")
}  

df <- df %>% mutate(is_internal=is_internal_host(src))

df %>% filter(is_internal) %>% group_by(src) %>% summarize(egress=sum(bytes)) %>% arrange(egress) %>% tail(1)

```
Ответ: 13.37.84.125 
### Вопрос 2: У другого злоумышленника запланировано задание по экспорту содержимого нашей внутренней вики. Один хост отправляет гораздо больше данных с предприятия в нерабочее время, чем другие, в отличие от хоста, указанного в вопросе 1. Каков его IP-адрес? 
```{r}
df %>% mutate(t=hour(rtime)) %>% group_by(t) %>% summarize(t_sum=sum(t))
```
```{r}
A tibble: 24 × 2
t t_sum
<int> <int>
0 0
1 494821
2 990244
3 1483083
4 1978764
5 2468125
6 2966406
7 3464804
8 4301112
9 4463172
10 4957650
11 5438411
12 5943804
13 6443580
14 6938372
15 7406805
16 195483936
17 207629891
18 220076226
19 232165142
20 244413420
21 256719141
22 269226606
23 281211225
```
Активное время работы компании с 16 до 24 часов.
```{r}
wdf <- df %>% mutate(t=hour(rtime))%>% filter(t > 15 & t < 24) 

wdf %>% filter(is_internal) %>% group_by(src) %>% summarize(egress=sum(bytes)) %>% arrange(egress) %>% tail(1)
```
Ответ: 13.37.84.125
