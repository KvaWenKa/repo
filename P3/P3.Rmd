---
title: "P3"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Основы обработки данных с помощью R


## Цель

1. Развить практические навыки использования языка программирования R для обработки данных
2. Закрепить знания базовых типов данных языка R
3. Развить пркатические навыки использования функций обработки данных пакета dplyr – функции
select(), filter(), mutate(), arrange(), group_by()

## Задание

Проанализировать встроенный в пакет dplyr наборданных starwars спомощью языка R и ответить на вопросы.

## Содержание ПР

Подключаем библиотеку dplyr.
```{r}
library(dplyr)
```

### 1. Сколько строк в датафрейме?

```{r}
starwars %>% nrow()
```

### 2. Сколько столбцов в датафрейме?

```{r}
starwars %>% ncol()
```

### 3. Как просмотреть примерный вид датафрейма?

```{r}
starwars %>% glimpse()
```
### 4. Сколько уникальных рас персонажей (species) представлено в данных?

```{r}
starwars %>% summarise(count = n_distinct(species)) %>% knitr::kable()
```

### 5. Найти самого высокого персонажа.

```{r}
starwars %>% filter(height == max(height, na.rm = TRUE)) %>% select(name) %>% knitr::kable()
```

### 6. Найти всех персонажей ниже 170

```{r}
starwars %>% filter(height < 170) %>% select(name) %>% knitr::kable()
```

### 7. Подсчитать ИМТ (индекс массы тела) для всех персонажей. ИМТ подсчитать по формуле 𝐼 = 𝑚/ℎ^2 , где 𝑚 – масса (weight), а ℎ – рост (height).

```{r}
starwars %>% mutate("imb" = mass/(height/100)^2) %>% select(name, imb)%>% knitr::kable()
```

### 8. Найти 10 самых “вытянутых” персонажей. “Вытянутость” оценить по отношению массы (mass) к росту (height) персонажей.

```{r}
starwars %>% mutate("longest" = mass/height) %>% arrange(longest) %>% slice(1:10) %>% select(name)%>% knitr::kable()
```

### 9. Найти средний возраст персонажей каждой расы вселенной Звездных войн.

```{r}
starwars %>% group_by(species) %>% summarise("middle age"= mean(birth_year, na.rm = TRUE))%>% knitr::kable()
```

### 10. Найти самый распространенный цвет глаз персонажей вселенной Звездных войн.

```{r}
starwars %>% group_by(eye_color) %>% summarise(count_eye_color = n()) %>% filter(count_eye_color == max(count_eye_color, na.rm = TRUE))%>% select(eye_color) %>% knitr::kable()
```

### 11. Подсчитать среднюю длину имени в каждой расе вселенной Звездных войн.

```{r}
starwars %>% group_by(species) %>% summarise("av_l"= mean(nchar(name), na.rm = TRUE))%>% knitr::kable()
```

## ️Вывод 

В результате выполнения работы были освоены основные инструменты пакета dplyr – функции

select(), filter(), mutate(), arrange(), group_by()

